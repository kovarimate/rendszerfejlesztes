﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Google.Cloud.Firestore;

namespace K86R7P
{
    public partial class Bejelentkezes : Form
    {
        FirestoreDb database;
        public Bejelentkezes()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + @"rendszerfejlesztes-firebase.json";
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", path);
            database = FirestoreDb.Create("rendszerfejlesztes-ff138");
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            bool succes = false;
            Query users = database.Collection("users");
            QuerySnapshot snap = await users.GetSnapshotAsync();
            foreach(DocumentSnapshot docsnap in snap.Documents)
            {
                User u = docsnap.ConvertTo<User>();
                if(u.userName==textBox1.Text && u.password == textBox2.Text)
                {
                    succes = true;
                    this.Hide();
                    if (u.job == "Eszkozfelelos")
                    {
                       Eszkozfelelos eszkozf = new Eszkozfelelos();
                       eszkozf.Show();
                    }
                    else
                    {
                        if (u.job=="Operator")
                        {
                            Operator op = new Operator();
                            op.Show();
                        }
                        else
                        {
                            if (u.job=="Karbantarto")
                            {
                                Karbantarto karb = new Karbantarto();
                                karb.Show();
                            }
                            else
                            {
                                MessageBox.Show("Hibás azonosítás, vegye fel a kapcsolatot a rendszergazdával!", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                this.Show();
                            }
                        }
                    }
                    break;
                }
            }
            if (succes == false)
            {
                MessageBox.Show("Hibás Adatok!", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
