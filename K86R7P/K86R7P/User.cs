﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Cloud.Firestore;

namespace K86R7P
{
    [FirestoreData]
    public class User
    {
        [FirestoreProperty]
        public int id { get; set; }

        [FirestoreProperty]
        public string job { get; set; }

        [FirestoreProperty]
        public string name { get; set; }

        [FirestoreProperty]
        public string password { get; set; }

        [FirestoreProperty]
        public string userName { get; set; }
    }
}
