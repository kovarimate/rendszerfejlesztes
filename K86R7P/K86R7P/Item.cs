﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Cloud.Firestore;


namespace K86R7P
{
    [FirestoreData]
    internal class Item
    {
        [FirestoreProperty]
        public int categoryId { get; set; }

        [FirestoreProperty]
        public int id { get; set; }

        [FirestoreProperty]
        public string location { get; set; }

        [FirestoreProperty]
        public string name { get; set; }
    }
}
