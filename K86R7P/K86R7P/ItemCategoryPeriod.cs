﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Cloud.Firestore;

namespace K86R7P
{
    [FirestoreData]
    internal class ItemCategoryPeriod
    {
        [FirestoreProperty]
        public int categoryId { get; set; }

        [FirestoreProperty]
        public string period { get; set; }
    }
}
