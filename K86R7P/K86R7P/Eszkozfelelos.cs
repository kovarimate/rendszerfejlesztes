﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Google.Cloud.Firestore;

namespace K86R7P
{
    public partial class Eszkozfelelos : Form
    {
        FirestoreDb database;
        public Eszkozfelelos()
        {
            InitializeComponent();
        }
        private async void Eszkozfelelos_Load(object sender, EventArgs e)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + @"rendszerfejlesztes-firebase.json";
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", path);
            database = FirestoreDb.Create("rendszerfejlesztes-ff138");

            Query itemCategory = database.Collection("itemCategory");
            QuerySnapshot snap = await itemCategory.GetSnapshotAsync();
            foreach (DocumentSnapshot docsnap in snap.Documents)
            {
                ItemCategory ic = docsnap.ConvertTo<ItemCategory>();
                comboBox1.Items.Add(ic.name);
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex > -1 && textBox1.Text != "")
            {
                Query itemCategory = database.Collection("itemCategory");
                QuerySnapshot snap = await itemCategory.GetSnapshotAsync();
                foreach (DocumentSnapshot docsnap in snap.Documents)
                {
                    ItemCategory ic = docsnap.ConvertTo<ItemCategory>();
                    if (comboBox1.SelectedItem.ToString() == ic.name)
                    {
                        ItemCategoryTime ict = new ItemCategoryTime() {categoryId=ic.id, time=int.Parse(textBox1.Text) };
                        DocumentReference addedDocRef = await database.Collection("itemCategoryTime").AddAsync(ict);
                        textBox1.Text = "";
                        break;
                    }
                }
            }
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex > -1 && comboBox2.SelectedIndex > -1)
            {
                Query itemCategory = database.Collection("itemCategory");
                QuerySnapshot snap = await itemCategory.GetSnapshotAsync();
                foreach (DocumentSnapshot docsnap in snap.Documents)
                {
                    ItemCategory ic = docsnap.ConvertTo<ItemCategory>();
                    if (comboBox1.SelectedItem.ToString() == ic.name)
                    {
                        ItemCategoryPeriod icp = new ItemCategoryPeriod() { categoryId = ic.id, period = comboBox2.SelectedItem.ToString() };
                        DocumentReference addedDocRef = await database.Collection("itemCategoryPeriod").AddAsync(icp);
                        break;
                    }
                }
            }
        }
    }
}
